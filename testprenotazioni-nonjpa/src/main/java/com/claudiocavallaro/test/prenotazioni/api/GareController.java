package com.claudiocavallaro.test.prenotazioni.api;

import com.claudiocavallaro.test.prenotazioni.service.GareService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GareController {

    /**
     * Questo controller fa il retrieve dei tipi di gare
     * Al momento non è prevista un'operazione di inserimento
     * Guardare le configurazioni iniziali del db per vedere la lista di gare prenotabili
     */

    @Autowired
    private GareService gareService;

    @GetMapping("/findallGare")
    public String findAllGare(){
        return gareService.findAll();
    }

    @GetMapping("/findGaraById")
    public String findGaraById(long id){
        return gareService.findById(id);
    }

    @GetMapping("/findGaraByType")
    public String findGaraByType(String gara){
        return gareService.findByGara(gara);
    }
}
