package com.claudiocavallaro.test.prenotazioni.persistence;

import com.claudiocavallaro.test.prenotazioni.model.Gare;
import lombok.extern.slf4j.Slf4j;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Slf4j
public class GareDAO {

    private static final String SELECT_ALL = "select * from db.gare";

    private static final String SELECT_BY_ID = " where id = ?";

    private static final String SELECT_BY_GARA = " where gara = ?";

    private static final GareDAO INSTANCE = new GareDAO();

    private GareDAO(){
    }

    public static GareDAO getInstance(){
        return INSTANCE;
    }

    public List<Gare> getAllGare(){
        final List<Gare> listaGare = new ArrayList<>();
        try(final Connection conn = DBManager.createConnection();
            final PreparedStatement statement = conn.prepareStatement(SELECT_ALL);){

            statement.execute();
            ResultSet result = statement.getResultSet();

            while(result.next()){
                final Gare gare = new Gare();
                gare.setId(result.getLong("id"));
                gare.setGara(result.getString("gara"));

                listaGare.add(gare);
            }

        } catch (SQLException e){
            log.error(e.getMessage());
        }
        return listaGare;
    }

    public Gare getGara(long id){
        Gare gara = null;
        try(final Connection conn = DBManager.createConnection();
            final PreparedStatement statement = conn.prepareStatement(SELECT_ALL + SELECT_BY_ID);){

            statement.setLong(1, id);
            statement.execute();
            ResultSet result = statement.getResultSet();

            while(result.next()){
                gara = new Gare();
                gara.setId(result.getLong("id"));
                gara.setGara(result.getString("gara"));
            }

        } catch (SQLException e){
            log.error(e.getMessage());
        }
        return gara;
    }

    public Gare getGaraByType(String gara){
        Gare garaToRetrieve = null;
        try(final Connection conn = DBManager.createConnection();
            final PreparedStatement statement = conn.prepareStatement(SELECT_ALL + SELECT_BY_GARA);){

            statement.setString(1, gara);
            statement.execute();
            ResultSet result = statement.getResultSet();

            while(result.next()){
                garaToRetrieve = new Gare();
                garaToRetrieve.setId(result.getLong("id"));
                garaToRetrieve.setGara(result.getString("gara"));
            }

        } catch (SQLException e){
            log.error(e.getMessage());
        }
        return garaToRetrieve;
    }
}
