package com.claudiocavallaro.test.prenotazioni.service;

import com.claudiocavallaro.test.prenotazioni.model.Atleta;
import com.claudiocavallaro.test.prenotazioni.persistence.PrenotazioneDAO;
import com.claudiocavallaro.test.prenotazioni.model.Gare;
import com.claudiocavallaro.test.prenotazioni.model.Prenotazione;
import com.claudiocavallaro.test.prenotazioni.persistence.GareDAO;
import com.claudiocavallaro.test.prenotazioni.persistence.AtletaDAO;
import com.google.gson.Gson;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;

@Service
public class PrenotazioneService {

    private AtletaDAO atletaDAO = AtletaDAO.getInstance();

    private GareDAO gareDAO = GareDAO.getInstance();

    private PrenotazioneDAO prenotazioneDAO = PrenotazioneDAO.getInstance();

    public List<Prenotazione> getRaceByType(String tipoGara){
        Gare gara = gareDAO.getGaraByType(tipoGara);
        if (gara != null){
            List<Prenotazione> listaGarePerTipo = prenotazioneDAO.getRaceByType(gara.getId());
            listaGarePerTipo.sort(new Comparator<Prenotazione>() {
                @Override
                public int compare(Prenotazione o1, Prenotazione o2) {
                    Integer sum1 = getTimeInMillis(o1.getTempo());
                    Integer sum2 = getTimeInMillis(o2.getTempo());
                    return sum2.compareTo(sum1);
                }
            });
            return listaGarePerTipo;
        } else {
            return null;
        }
    }

    public String getRaceByAtleta(String atletaString){
        Atleta atleta = atletaDAO.getByName(atletaString);
        if (atleta != null){
            return new Gson().toJson(prenotazioneDAO.getRaceByUser(atleta.getId()));
        } else {
            return toJson("Problem with inputs");
        }
    }

    public String insert(Prenotazione prenotazione){
        boolean checkTime = checkRaceTime(prenotazione.getTempo());
        if (checkTime == false){
            return toJson("Problem with inputs");
        }
        Atleta atleta = atletaDAO.getByName(prenotazione.getNome());
        Gare gara = gareDAO.getGaraByType(prenotazione.getGara());
        if (atleta != null && gara != null){
            boolean flag = prenotazioneDAO.insert(atleta.getId(), gara.getId(), prenotazione.getTempo());
            if (flag){
                return toJson("Successfull");
            } else {
                return toJson("Not inserted");
            }
        } else {
            return toJson("Problem with inputs");
        }
    }

    private boolean checkRaceTime(String tempo) {
        String[] splits = tempo.split(":");
        if (splits.length < 3){
            return  false;
        }
        for (int i = 0; i < splits.length ; i++){
            String s = splits[i];
            if (s.matches("[A-Za-z]+")){
                return false;
            }
        }
        return true;
    }

    private Integer getTimeInMillis(String tempo){
        String[] splits = tempo.split(":");
        Integer minuti = Integer.valueOf(splits[0]);
        Integer secondi = Integer.valueOf(splits[1]);
        Integer millisecondi = Integer.valueOf(splits[2]);

        Integer sum = millisecondi + (secondi * 1000) + (minuti * 60 * 1000);
        return sum;
    }

    private String toJson(String val){
        return new Gson().toJson(val);
    }
}
