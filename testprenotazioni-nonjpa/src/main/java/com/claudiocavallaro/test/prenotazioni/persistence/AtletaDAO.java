package com.claudiocavallaro.test.prenotazioni.persistence;

import com.claudiocavallaro.test.prenotazioni.model.Atleta;
import lombok.extern.slf4j.Slf4j;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Slf4j
public class AtletaDAO {

    private static final AtletaDAO INSTANCE = new AtletaDAO();

    private static final String SELECT_ALL = "select * from db.atleta;";

    private static final String SELECT_BY_NAME = "select * from db.atleta where nome = ?";

    private static final String INSERT = "insert into db.atleta(nome, anno, sesso) values (?, ?, ?);";

    private AtletaDAO() {
    }

    public static AtletaDAO getInstance() {
        return INSTANCE;
    }

    public boolean insert(Atleta atleta) {
        boolean flag = true;
        try (final Connection conn = DBManager.createConnection();
             final PreparedStatement statement = conn.prepareStatement(INSERT);) {

            statement.setString(1, atleta.getNome());
            statement.setString(2, atleta.getAnno());
            statement.setString(3, atleta.getSesso());
            statement.execute();

        } catch (SQLException s) {
            log.error(s.getMessage());
            flag = false;
        }
        return flag;
    }

    public List<Atleta> get() {
        final List<Atleta> atleti = new ArrayList<>();
        try (final Connection conn = DBManager.createConnection();
             final PreparedStatement statement = conn.prepareStatement(SELECT_ALL);) {

            statement.execute();
            ResultSet result = statement.getResultSet();

            while (result.next()) {
                final Atleta atleta = new Atleta();
                atleta.setId(result.getLong("id"));
                atleta.setNome(result.getString("nome"));
                atleta.setAnno(result.getString("anno"));
                atleta.setSesso(result.getString("sesso"));

                atleti.add(atleta);

            }
        } catch (SQLException s) {
            log.error(s.getMessage());
        }

        return atleti;
    }

    public Atleta getByName(String name) {
        Atleta atleta = null;
        try (final Connection conn = DBManager.createConnection();
             final PreparedStatement statement = conn.prepareStatement(SELECT_BY_NAME);) {

            statement.setString(1, name);
            statement.execute();
            ResultSet resultSet = statement.getResultSet();
            while (resultSet.next()){
                atleta = new Atleta();
                atleta.setId(resultSet.getLong("id"));
                atleta.setNome(resultSet.getString("nome"));
                atleta.setSesso(resultSet.getString("sesso"));
                atleta.setAnno(resultSet.getString("anno"));
            }

        } catch (SQLException e) {
            log.error(e.getMessage());
        }
        return atleta;
    }

}
