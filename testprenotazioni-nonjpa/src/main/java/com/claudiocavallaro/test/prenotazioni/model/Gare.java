package com.claudiocavallaro.test.prenotazioni.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Gare {

    /**
     * Questa entity è per rappresentare i tipi di gare
     */

    private long id;

    @JsonProperty("gara")
    private String gara;

    public Gare(){}

    public Gare(long id, String gara) {
        this.id = id;
        this.gara = gara;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getGara() {
        return gara;
    }

    public void setGara(String gara) {
        this.gara = gara;
    }
}
