package com.claudiocavallaro.test.prenotazioni.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Prenotazione {

    /**
     * Questa entity è per rappresentare il concetto di prenotazione alla gara
     */

    private long id;

    @JsonProperty("nome")
    private String nome;

    @JsonProperty("gara")
    private String gara;

    /**
     * Il tempo deve essere nella forma
     * minuti:secondi:centesimi
     * Se per esempio bisogna inserire 32.5 secondi scrivere
     * 0:32:5
     */
    @JsonProperty("tempo")
    private String tempo;

    public Prenotazione() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getGara() {
        return gara;
    }

    public void setGara(String gara) {
        this.gara = gara;
    }

    public String getTempo() {
        return tempo;
    }

    public void setTempo(String tempo) {
        this.tempo = tempo;
    }
}
