package com.claudiocavallaro.test.prenotazioni.service;

import com.claudiocavallaro.test.prenotazioni.persistence.GareDAO;
import com.claudiocavallaro.test.prenotazioni.model.Gare;
import com.google.gson.Gson;
import org.springframework.stereotype.Service;

@Service
public class GareService {

    private GareDAO gareDAO = GareDAO.getInstance();

    public String findAll() {
        return new Gson().toJson(gareDAO.getAllGare());
    }

    public String findById(long id) {
        Gare toReturn = gareDAO.getGara(id);
        if (toReturn != null) {
            return new Gson().toJson(toReturn);
        } else {
            return "Nessun risultato";
        }
    }

    public String findByGara(String gara) {
        Gare toReturn = gareDAO.getGaraByType(gara);
        if (toReturn != null){
            return new Gson().toJson(toReturn);
        } else {
            return "Nessun risultato";
        }
    }

}
