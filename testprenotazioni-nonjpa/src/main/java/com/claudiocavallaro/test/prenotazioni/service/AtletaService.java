package com.claudiocavallaro.test.prenotazioni.service;

import com.claudiocavallaro.test.prenotazioni.model.Atleta;
import org.springframework.stereotype.Service;

import com.claudiocavallaro.test.prenotazioni.persistence.AtletaDAO;
import com.google.gson.Gson;

@Service
public class AtletaService {
	
	private AtletaDAO atletaDAO = AtletaDAO.getInstance();
	
	public String findAll() {
		return new Gson().toJson(atletaDAO.get());
	}
	
	public String insert(Atleta atleta) {
		boolean result = atletaDAO.insert(atleta);
		if (result){
			return new Gson().toJson("Successfull");
		} else {
			return new Gson().toJson("Not inserted");
		}
	}

	public String findByName(String nome){
		Atleta atleta = atletaDAO.getByName(nome);
		if (atleta != null){
			return new Gson().toJson(atleta);
		} else {
			return new Gson().toJson("Nessun risultato");
		}
	}

}
