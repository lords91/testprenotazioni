package com.claudiocavallaro.test.prenotazioni.service;

import com.claudiocavallaro.test.prenotazioni.model.Prenotazione;
import org.apache.commons.collections4.ListUtils;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileOutputStream;
import java.util.List;

@Service
public class FileService {

    @Autowired
    private PrenotazioneService prenotazioneService;

    public File createExcel(String gara) throws Exception {
        List<Prenotazione> listaGare = prenotazioneService.getRaceByType(gara);
        if (listaGare != null && !listaGare.isEmpty()){
            Workbook workbook = new XSSFWorkbook();
            Sheet sheet = workbook.createSheet("Test prenotazioni");

            Row header = sheet.createRow(0);
            CellStyle headerStyle = workbook.createCellStyle();
            headerStyle.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());
            headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

            XSSFFont font = ((XSSFWorkbook) workbook).createFont();
            font.setFontName("Arial");
            font.setFontHeightInPoints((short) 16);
            font.setBold(true);
            headerStyle.setFont(font);

            Cell headerCell = header.createCell(0);
            headerCell.setCellValue("50 stile");
            headerCell.setCellStyle(headerStyle);

            CellStyle style = workbook.createCellStyle();
            style.setWrapText(true);

            List<List<Prenotazione>> liste = ListUtils.partition(listaGare, 6);

            int offset = 0;
            for (int i = 0; i < liste.size(); i++){
                List<Prenotazione> subList = liste.get(i);
                Row rowBatteria = sheet.createRow(3+offset);
                Cell cellBatteria = rowBatteria.createCell(0);
                cellBatteria.setCellValue("Batteria " + (i+1));

                Row rowHeader = sheet.createRow(5+offset);
                Cell cellHeader = rowHeader.createCell(0);
                cellHeader.setCellValue("Corsia");

                cellHeader = rowHeader.createCell(1);
                cellHeader.setCellValue("Nome");

                cellHeader = rowHeader.createCell(2);
                cellHeader.setCellValue("Tempo");

                for (int j = 0; j < subList.size(); j++){
                    Row row = sheet.createRow(j+7+offset);
                    Cell cell = row.createCell(0);
                    cell.setCellValue(j+1);
                    cell.setCellStyle(style);

                    cell = row.createCell(1);
                    cell.setCellValue(subList.get(j).getNome());
                    cell.setCellStyle(style);

                    cell = row.createCell(2);
                    cell.setCellValue(subList.get(j).getTempo());
                    cell.setCellStyle(style);
                }
                offset = offset + 11;
            }

            File currDir = new File(".");
            String path = currDir.getAbsolutePath();
            String fileLocation = path.substring(0, path.length() - 1) + "batterie_"+ gara +".xlsx";

            FileOutputStream outputStream = new FileOutputStream(fileLocation);
            workbook.write(outputStream);
            workbook.close();

            return new File(fileLocation);
        } else {
            throw new Exception("Non esiste alcuna gara");
        }
    }

}
