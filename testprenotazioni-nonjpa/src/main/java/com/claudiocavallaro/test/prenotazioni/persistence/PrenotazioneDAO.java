package com.claudiocavallaro.test.prenotazioni.persistence;

import com.claudiocavallaro.test.prenotazioni.model.Prenotazione;
import lombok.extern.slf4j.Slf4j;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Slf4j
public class PrenotazioneDAO {

    private static final PrenotazioneDAO INSTANCE = new PrenotazioneDAO();

    private static final String INSERT = "insert into db.prenotazione(atleta, gara, tempo) values (?, ?, ?)";

    private static final String SELECT = "select  u.nome, g.gara , r.tempo, r.id " +
            " from db.prenotazione r join " +
            " db.gare g on r.gara = g.id join " +
            " db.atleta u on r.atleta = u.id " ;

    private static final String BY_ATLETA = " where r.atleta = ? ";

    private static final String BY_GARA = " where r.gara = ? ";

    private PrenotazioneDAO(){

    }

    public static PrenotazioneDAO getInstance(){
        return INSTANCE;
    }

    public boolean insert(long atleta, long gara, String tempo){
        boolean flag = true;
        try (final Connection conn = DBManager.createConnection();
             final PreparedStatement statement = conn.prepareStatement(INSERT);) {
            statement.setLong(1, atleta);
            statement.setLong(2, gara);
            statement.setString(3, tempo);

            statement.execute();
        } catch (SQLException e){
            log.error(e.getMessage());
            flag = false;
        }
        return flag;
    }

    public List<Prenotazione> getRaceByUser(long atleta){
        List<Prenotazione> listaGare = new ArrayList<>();
        try (final Connection conn = DBManager.createConnection();
             final PreparedStatement statement = conn.prepareStatement(SELECT + BY_ATLETA);) {

            statement.setLong(1, atleta);
            statement.execute();
            ResultSet resultSet = statement.getResultSet();
            while (resultSet.next()){
                final Prenotazione prenotazione = new Prenotazione();
                prenotazione.setId(resultSet.getLong("id"));
                prenotazione.setNome(resultSet.getString("nome"));
                prenotazione.setGara(resultSet.getString("gara"));
                prenotazione.setTempo(resultSet.getString("tempo"));

                listaGare.add(prenotazione);
            }

        } catch (SQLException e){
            log.error(e.getMessage());
        }
        return listaGare;
    }

    public List<Prenotazione> getRaceByType(long gara){
        List<Prenotazione> listaGare = new ArrayList<>();
        try (final Connection conn = DBManager.createConnection();
             final PreparedStatement statement = conn.prepareStatement(SELECT + BY_GARA);) {

            statement.setLong(1, gara);
            statement.execute();
            ResultSet resultSet = statement.getResultSet();
            while (resultSet.next()){
                final Prenotazione prenotazione = new Prenotazione();
                prenotazione.setId(resultSet.getLong("id"));
                prenotazione.setNome(resultSet.getString("nome"));
                prenotazione.setGara(resultSet.getString("gara"));
                prenotazione.setTempo(resultSet.getString("tempo"));

                listaGare.add(prenotazione);
            }

        } catch (SQLException e){
            log.error(e.getMessage());
        }
        return listaGare;
    }
}
