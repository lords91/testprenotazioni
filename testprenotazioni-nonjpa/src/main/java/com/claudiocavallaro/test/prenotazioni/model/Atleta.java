package com.claudiocavallaro.test.prenotazioni.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Atleta {

	private long id;

	@JsonProperty("nome")
	private String nome;

	@JsonProperty("anno")
	private String anno;

	/**
	 * Possibili valori:
	 * M / F
	 */
	@JsonProperty("sesso")
	private String sesso;

	public Atleta() {
	}

	public Atleta(long id, String nome, String anno, String sesso) {
		this.id = id;
		this.nome = nome;
		this.anno = anno;
		this.sesso = sesso;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getAnno() {
		return anno;
	}

	public void setAnno(String anno) {
		this.anno = anno;
	}

	public String getSesso() {
		return sesso;
	}

	public void setSesso(String sesso) {
		this.sesso = sesso;
	}

	@Override
	public String toString() {
		return super.toString();
	}
}