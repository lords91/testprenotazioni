package com.claudiocavallaro.test.prenotazioni.api;

import com.claudiocavallaro.test.prenotazioni.model.Prenotazione;
import com.claudiocavallaro.test.prenotazioni.service.PrenotazioneService;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class RaceController {

    @Autowired
    private PrenotazioneService prenotazioneService;

    @RequestMapping(value = "/addPrenotazione", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public String addPrenotazione(@RequestBody Prenotazione prenotazione) {
        return prenotazioneService.insert(prenotazione);
    }

    @GetMapping("/findPrenotazioneAtleta")
    public String findPrenotazioneAtleta(String atleta){
        return prenotazioneService.getRaceByAtleta(atleta);
    }

    @GetMapping("/findPrenotazioneByType")
    public String findPrenotazioneByType(String gara){
        List<Prenotazione> lista = prenotazioneService.getRaceByType(gara);
        if (lista == null){
            return "Problems with inputs";
        } else {
            return new Gson().toJson(lista);
        }
    }
}
