package com.claudiocavallaro.test.prenotazioni.api;

import com.claudiocavallaro.test.prenotazioni.model.Atleta;
import com.claudiocavallaro.test.prenotazioni.service.AtletaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class AtletaController {

    @Autowired
    private AtletaService atletaService;

    @GetMapping("/findallAtleta")
    public String findAll() {
        return atletaService.findAll();
    }

    @GetMapping("/findAtletaByName")
    public String findAtletaByName(String name) {
        return atletaService.findByName(name);
    }

    @RequestMapping(value = "/addAtleta", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public String addAtleta(@RequestBody Atleta atleta) {
        return atletaService.insert(atleta);
    }
}
