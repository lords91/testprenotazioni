create schema db;

CREATE table "db".atleta(
	id serial primary key,
	nome varchar(255) not null,
	sesso varchar(1) not null check(sesso in('M', 'F')),
	anno varchar(20) not null,
	unique(nome, sesso, anno)
);
CREATE INDEX atleta_DB_ID
  ON "db".atleta (ID);

Create sequence "db".atleta_id
start 1
increment 1
owned by "db".atleta.id;

insert into "db".atleta(
    nome, anno, sesso)
values ('nome cognome', '1991', 'M');
-------------------------------------------------------------
create table db.gare(
	id serial primary key,
	gara varchar(50) not null check(gara in (
	'50 stile', '100 stile', '200 stile', '400 stile',
	'50 dorso', '100 dorso', '200 dorso',
	'50 rana', '100 rana', '200 rana',
	'50 farfalla', '100 farfalla', '200 farfalla',
	'100 misto', '200 misto'
	))
);
CREATE INDEX GARE_DB_ID
  ON "db".gare (ID);

Create sequence "db".gare_id
start 1
increment 1
owned by "db".gare.id;

insert into "db".gare(
    gara)
values ('50 stile'),('100 stile'),('200 stile'),('400 stile'),
	('50 dorso'), ('100 dorso'), ('200 dorso'),
	('50 rana'), ('100 rana'),('200 rana'),
	('50 farfalla'), ('100 farfalla'), ('200 farfalla'),
	('100 misto'), ('200 misto');
-------------------------------------------------------------

create table db.prenotazione(
	id serial primary key,
	atleta serial not null,
	gara serial not null,
	tempo varchar(10),
	foreign key (gara) references db.gare(id),
	foreign key (atleta) references db.atleta(id),
	unique(atleta, gara, tempo)
);