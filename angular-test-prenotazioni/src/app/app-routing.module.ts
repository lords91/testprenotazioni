import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AtletaComponent } from './atleta/atleta.component';
import { RaceComponent } from './race/race.component';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'addUser', component: AtletaComponent },
  { path: 'addRace',  component: RaceComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
