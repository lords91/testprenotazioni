import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AtletaComponent } from './atleta/atleta.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RaceComponent } from './race/race.component';


@NgModule({
  declarations: [
    AppComponent,
    AtletaComponent,
    RaceComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
