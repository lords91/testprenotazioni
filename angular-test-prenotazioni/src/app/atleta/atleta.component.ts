import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormControl, FormGroup } from '@angular/forms';
import { Atleta } from './atleta';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-atleta',
  templateUrl: './atleta.component.html',
  styleUrls: ['./atleta.component.css']
})
export class AtletaComponent implements OnInit {
  postId: any;
  nomeString : string = '';
  anno : string = '';
  sex : string = '';
  esito : string = '';
  baseURL: string = "http://localhost:8080/";

  form = new FormGroup({
    gender: new FormControl(''),
    nome: new FormControl(''),
    cognome: new FormControl(''),
    day: new FormControl(''),
    month: new FormControl(''),
    year: new FormControl('')
  });

  constructor(private http: HttpClient) { }

  ngOnInit() {      
  }

  onSubmit(): void{
    this.nomeString = this.form.value.nome + ' ' + this.form.value.cognome
    this.sex = this.form.value.gender;
    this.anno = this.form.value.day + ' ' + this.form.value.month + ' ' + this.form.value.year
    let atleta:Atleta = new Atleta(this.nomeString, this.sex, this.anno);
    console.log(JSON.stringify(atleta))
    this.doPost(atleta).subscribe(data =>{
      this.esito = data;
    });
  }

  doPost(atleta:Atleta): Observable<any>{
    const headers = {
      'Content-Type': 'application/json',
      'Accept' : '*/*'
    }  
    const body=JSON.stringify(atleta);
    console.log(body)
    return this.http.post(this.baseURL + 'addAtleta', body,{'headers':headers})}

}
