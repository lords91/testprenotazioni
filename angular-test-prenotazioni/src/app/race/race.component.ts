import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormControl, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { Race } from './race';

@Component({
  selector: 'app-race',
  templateUrl: './race.component.html',
  styleUrls: ['./race.component.css']
})
export class RaceComponent implements OnInit {
  esito : string = '';
  baseURL: string = "http://localhost:8080/";

  form = new FormGroup({
    nome: new FormControl(''),
    gara: new FormControl(''),
    tempo: new FormControl('')
  });

  constructor(private http: HttpClient) { }

  ngOnInit() {      
  }

  onSubmit(): void{
    let race : Race = new Race(this.form.value.nome, this.form.value.gara, this.form.value.tempo);
    this.doPost(race).subscribe(data => {
      this.esito = data;
    });
  }

  doPost(race:Race): Observable<any>{
    const headers = { 'content-type': 'application/json'}  
    const body=JSON.stringify(race);
    console.log(body)
    return this.http.post(this.baseURL + 'addPrenotazione', body,{'headers':headers})}
}
