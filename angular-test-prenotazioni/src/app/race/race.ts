export class Race {
    private nome: string;
    private gara: string;
    private tempo: string;

    constructor(nome: string, gara:string, tempo:string){
        this.nome = nome;
        this.gara = gara;
        this.tempo = tempo;
    }
}