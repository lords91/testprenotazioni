# testPrenotazioni
Requisiti:
- docker
- docker- compose
- maven

Requisiti Angular:
- docker
- docker-compose
- nodejs con angular cli installata

Per far partire la sola instanza del database basta eseguire ./start_db.sh
Una volta eseguita quella si può avviare l'applicazione SpringBoot anche tramite l'IDE importanto il progetto come maven project.

Una volta avviata l'applicazione si avvia il web server a localhost:8080
Per tutte le possibili operation andare a visualizzare i controller.

Per startare il tutto:
1) `./start.sh` nella cartella testprenotazioni-nonjpa (avvia sia il db che il backend)
2) `./docker_start.sh` nella cartella angular-test-prenotazioni (avvia il frontend)
 
 
 >Il tempo deve essere nel formato minuti:secondi:centesimi.
 Se per esempio bisogna rappresentare 32.5 secondi basta mettere 0:32:5
